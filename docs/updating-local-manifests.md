# Updating manifests from latest release

```bash
rm -rf manifests/* fluentd/
helm repo add elastic https://helm.elastic.co
helm repo update
helm fetch elastic/elasticsearch --untar
helm template elasticsearch \
    --output-dir manifests \
    --namespace elasticsearch \
    elasticsearch/
rm -rf manifests/elasticsearch/templates/test
cp -r manifests/elasticsearch/templates/* manifests/
pushd manifests/elasticsearch/templates/
for n in *; do printf '%s\n' "- $n"; done > ../../tmp-resources
cd ../../
cat <<EOF > kustomization.yml
resources:
$(cat tmp-resources)
EOF
popd
rm -rf manifests/elasticsearch manifests/tmp-resources elasticsearch
```